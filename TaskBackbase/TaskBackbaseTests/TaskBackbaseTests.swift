//
//  TaskBackbaseTests.swift
//  TaskBackbaseTests
//
//  Created by Dimitar Shopovski on 9/25/18.
//  Copyright © 2018 Dimitar Shopovski. All rights reserved.
//

import XCTest
import MapKit
@testable import TaskBackbase

class TaskBackbaseTests: XCTestCase {

    var systemUnderTest: CitiesViewController!

    override func setUp() {

        super.setUp()
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        systemUnderTest = storyboard.instantiateViewController(withIdentifier: "citiesVC") as? CitiesViewController
        _ = systemUnderTest.view

    }

    override func tearDown() {

        systemUnderTest = nil
        
        super.tearDown()
    }

    func testSUT_ShouldSetSearchBarDelegate() {
        
        XCTAssertNotNil(self.systemUnderTest.citySearchBar.delegate)
    }

    func testSUT_ConformsToSearchBarDelegateProtocol() {
        
        XCTAssert(systemUnderTest.conforms(to: UISearchBarDelegate.self))
        XCTAssertTrue(self.systemUnderTest.responds(to: #selector(systemUnderTest.searchBarTextDidBeginEditing(_:))))
        XCTAssertTrue(self.systemUnderTest.responds(to: #selector(systemUnderTest.searchBarSearchButtonClicked(_:))))
    }
    
    func testSUT_CanProperlyDisplay_Cities_AfterSearchTextChanges() {
        
        // exampleCities
        let cityStruga = City(name: "Struga", country: "MK", id: 32, coordinates: CLLocationCoordinate2DMake(32.00, 4.00))
        let cityStrugari = City(name: "Strugari", country: "RO", id: 33, coordinates: CLLocationCoordinate2DMake(32.00, 4.00))
        let cityStrugiK = City(name: "Strugi-Krasnyye", country: "RU", id: 34, coordinates: CLLocationCoordinate2DMake(32.00, 4.00))

        
        systemUnderTest.listOfCities = [cityStruga, cityStrugari, cityStrugiK]
        
        // simulate user typing in Search text and confirm results ...
        
        systemUnderTest.searchBar(systemUnderTest.citySearchBar, textDidChange: "Strug")
        XCTAssertEqual(systemUnderTest.filtered.count, 3)
        XCTAssertEqual(systemUnderTest.filtered[0].name, "Struga")
        XCTAssertEqual(systemUnderTest.filtered[1].name, "Strugari")
        XCTAssertEqual(systemUnderTest.filtered[2].name, "Strugi-Krasnyye")
        
        systemUnderTest.searchBar(systemUnderTest.citySearchBar, textDidChange: "Struga")
        XCTAssertEqual(systemUnderTest.filtered.count, 2)
        XCTAssertEqual(systemUnderTest.filtered[0].name, "Struga")
        XCTAssertEqual(systemUnderTest.filtered[1].name, "Strugari")

    }

}
