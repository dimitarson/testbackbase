//
//  DetailViewController.swift
//  TaskBackbase
//
//  Created by Dimitar Shopovski on 9/25/18.
//  Copyright © 2018 Dimitar Shopovski. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!

    var city: City
    
    //MARK: View Controller life cycle
    init(city: City) {
        
        self.city = city
        super.init(nibName: "DetailViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = city.name

        createPin()
        setRegion()
    }
    
    //MARK: - Create pin
    fileprivate func createPin() {
        
        let cityPin = MKPointAnnotation()
        cityPin.coordinate = CLLocationCoordinate2D(latitude: city.location.latitude, longitude: city.location.longitude)
        cityPin.title = city.name
        cityPin.subtitle = city.country
        
        mapView.addAnnotation(cityPin)
    }
    
    private func setRegion() {
        
        let center = CLLocationCoordinate2D(latitude: city.location.latitude, longitude: city.location.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        //set region on the map
        mapView.setRegion(region, animated: true)
    }
}
