//
//  ViewController.swift
//  TaskBackbase
//
//  Created by Dimitar Shopovski on 9/25/18.
//  Copyright © 2018 Dimitar Shopovski. All rights reserved.
//

import UIKit

class CitiesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    var listOfCities: [City] = []
    var searchActive: Bool = false
    var filtered: [City] = []

    let pageSize = 30
    var fetchedElements = 0
    var totalElements = 0
    
    let statusBarHeight: CGFloat = 20.0

    @IBOutlet weak var citiesTableView: UITableView!
    @IBOutlet weak var progressHud: UIView!
    @IBOutlet weak var progressHudLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var citySearchBar: UISearchBar!
    @IBOutlet weak var searchBottomSpace: NSLayoutConstraint!

    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Cities"
        
        setupProgressHud()
        setupTableView()
        addKeyboardObservers()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.loadData()
        }
    }
    
    deinit {
        removeKeyboardObservers()
    }
    
    //MARK: - Setup UI
    private func setupTableView() {
        
        citiesTableView.register(UINib(nibName: CityTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: CityTableViewCell.cellIdentifier)
        citySearchBar.placeholder = "City name"
    }
    
    private func setupProgressHud() {
        
        progressHud.isHidden = false
        progressHud.layer.cornerRadius = 6.0
        progressHud.clipsToBounds = true
        progressHud.backgroundColor = .lightGray
        activityIndicator.startAnimating()
        progressHudLabel.text = "Fetching cities..."
    }
    
    //MARK: - Data Manager
    private func loadData() {
        
        DataManager.shared.fetchDataFromLocalFile(fileName: "cities", type: "json", onSuccess: { (listOfCities) in
            
            self.listOfCities = listOfCities.sorted()
            self.totalElements = listOfCities.count
            self.reloadSource()
            self.progressHud.isHidden = true
            
        }) { (error) in
            
            let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            
            self.progressHud.isHidden = true
        }
    }
    
    private func reloadSource() {
        
        if fetchedElements+pageSize <= totalElements {
            fetchedElements+=pageSize
        }
        else {
            fetchedElements = totalElements
        }
        
        citiesTableView.reloadData()
    }

    //MARK: - TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchActive {
            
            return filtered.count
        }
        
        return fetchedElements
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CityTableViewCell.cellIdentifier, for: indexPath) as! CityTableViewCell
        
        let city: City!
        if searchActive, indexPath.row < filtered.count {
            city = filtered[indexPath.row]
        }
        else {
            city = listOfCities[indexPath.row]
        }

        cell.setup(city: city)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCity: City!
        if searchActive {
            selectedCity = filtered[indexPath.row]
        }
        else {
            selectedCity = listOfCities[indexPath.row]
        }
        
        let detailController = DetailViewController.init(city: selectedCity)
        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if !searchActive {
            if indexPath.row == fetchedElements - 1 { // last cell
                if totalElements > fetchedElements { // more items to fetch
                    reloadSource() // increment totalitems
                }
            }
        }
    }
    
    //MARK: - Search bar delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = listOfCities.filter {
            ($0.name.lowercased().hasPrefix(searchText.lowercased()))
        }
        
        self.citiesTableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        
        doneSearchAction()
    }
    
    // MARK: - Keyboard handling
    
    private func addKeyboardObservers() {
        
        let notifications = NotificationCenter.default
        notifications.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        notifications.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func removeKeyboardObservers() {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
          if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval {
            
            guard let keyboardBeginFrame = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue else { return }
            guard let keyboardEndFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }

            
            let screenHeight = UIScreen.main.bounds.height
            let isBeginOrEnd = keyboardBeginFrame.origin.y == screenHeight || keyboardEndFrame.origin.y == screenHeight
            let heightOffset = keyboardBeginFrame.origin.y - keyboardEndFrame.origin.y - (isBeginOrEnd ? bottomLayoutGuide.length : 0)
            
            searchBottomSpace.constant = heightOffset
            
            UIView.animate(withDuration: duration) {
                
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if searchBottomSpace.constant > 0 {
            
            searchBottomSpace.constant = 0
            
            UIView.animate(withDuration: 0.5) {
                
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func doneSearchAction() {
    
        citySearchBar.text = ""
        citySearchBar.resignFirstResponder()
        citiesTableView.reloadData()
    }

}

