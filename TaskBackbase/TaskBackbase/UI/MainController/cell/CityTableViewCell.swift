//
//  CityTableViewCell.swift
//  TaskBackbase
//
//  Created by Dimitar Shopovski on 9/25/18.
//  Copyright © 2018 Dimitar Shopovski. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {

    static let nibName = "CityTableViewCell"
    static let cellIdentifier = "cityCellIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    private func setupUI() {
        
        accessoryType = .disclosureIndicator
    }

    func setup(city: City) {
        
        textLabel?.text = city.name + ", " + city.country
    }
}
