//
//  DataManager.swift
//  TaskBackbase
//
//  Created by Dimitar Shopovski on 9/25/18.
//  Copyright © 2018 Dimitar Shopovski. All rights reserved.
//

/*

 The local json file will be preprocess into an array of City objects.
 This will allow more easier access to the properties (name, country...) instead of using
 KVC if we use Dictionaries.
 
 */

import Foundation

class DataManager {
    
    static let shared = DataManager()
    
    func fetchDataFromLocalFile(fileName: String, type: String, onSuccess: @escaping([City]) -> Void, onFailure: @escaping(Error) -> Void) {
        
        if let path = Bundle.main.path(forResource: fileName, ofType: type) {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                
                do {
                    let decoder = JSONDecoder()
                    let cities = try decoder.decode([City].self, from: data)
                    
                    onSuccess(cities)
                    
                } catch {
                    onFailure(error)
                }
                
            } catch {

                onFailure(error)
            }
        }
    }
}
