//
//  City.swift
//  TaskBackbase
//
//  Created by Dimitar Shopovski on 9/25/18.
//  Copyright © 2018 Dimitar Shopovski. All rights reserved.
//

import Foundation
import MapKit

struct City: Decodable {
    
    let name: String
    let country: String
    let id: UInt
    let location: CLLocationCoordinate2D

    struct Location: Decodable {
        
        let latitude: Double
        let longitude: Double
        
        private enum CodingKeys : String, CodingKey {
        
            case latitude = "lat"
            case longitude = "lon"
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case name
        case country
        case id = "_id"
        case coordinates = "coord"
    }
    
    typealias CityLocation = City.Location
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try values.decode(String.self, forKey: .name)
        self.country = try values.decode(String.self, forKey: .country) 
        self.id = try values.decode(UInt.self, forKey: .id)
        
        let locationModel = try values.decode(Location.self, forKey: .coordinates)
        self.location = CLLocationCoordinate2D(cityLocation: locationModel)
    }
    
    init(name: String, country: String, id: UInt, coordinates: CLLocationCoordinate2D) {
        
        self.name = name
        self.country = country
        self.id = id
        self.location = coordinates
    }
}

extension City: Comparable {
    static func < (lhs: City, rhs: City) -> Bool {
        return lhs.name < rhs.name
    }
    
    static func == (lhs: City, rhs: City) -> Bool {
        return lhs.name == rhs.name
    }
}

extension CLLocationCoordinate2D {
    init(cityLocation: City.Location) {
        self.init(latitude: cityLocation.latitude, longitude: cityLocation.longitude)
    }
}
